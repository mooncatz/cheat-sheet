# Clone repo with 2FA using JetBrains IDEs

When you have 2FA enabled on your gitlab/github/bitbucket/whatever account and want to clone a repository using one of JetBrains IDEs it will not let you enter the 2FA code but instead fail signing in. This is because the way git work. Here is how you clone a repo with git with JetBrains IDEs. 

## 1. Create an access token for the IDE

* This steps are similar on other platforms as well. I will follow GitLab instuctions.  

* Create an access token to your account. Follow these steps: [creating-a-personal-access-token](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html#creating-a-personal-access-token)
  * Log in to GitLab.
  * In the upper-right corner, click your avatar and select `Settings`.
  * On the User Settings menu, select `Access Tokens`.
  * Set a name and optional expiry date for the token.
  * Choose the desired scopes, `api` will be enough if you do not need special configurations.
  * Click the `Create personal access token` button.
  * Save (or just copy once) the personal access token somewhere safe. Once you leave or refresh the page, you won’t be able to access it again.


## 2. Clone the repo with the IDE

* Clone the repo just as you would do normally.
  * For example from the `Start screen` choose `Check out from Version Control` --> `Git` --> enter the `URL` --> `Clone`
* When the IDE asks for your login credidentials there are two alternatives.
  * Username = your_username, Password = your_access_token
  * Username = your_access_token, Password= (leave empty)
* For more information [How-to-access-GIT-remote-repositories-with-2-factor-authentication](https://intellij-support.jetbrains.com/hc/en-us/articles/206537004-How-to-access-GIT-remote-repositories-with-2-factor-authentication)
