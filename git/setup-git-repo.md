> These instructions are copied from GitLab's instructions

## Command line instructions

You can upload existing files from your computer using the instructions below.

#### Git setup

```
git config user.name "username"
git config user.email "email@mail.com"
```

#### Clone a new repository (to your local machine) from remote

```
git clone https://gitlab.com/mooncatz/repository-name.git
cd repository-name
touch README.md
git add README.md
git commit -m "add README"
git push -u origin master
```

#### Push an existing Git repository to remote

```
cd existing_repo
git remote rename origin old-origin
git remote add origin https://gitlab.com/mooncatz/repository-name.git
git push -u origin --all
git push -u origin --tags
```

#### Push an existing folder to remote

```
cd existing_folder
git init
git remote add origin https://gitlab.com/mooncatz/repository-name.git
git add .
git commit -m "Initial commit"
git push -u origin master
```

## Commit and push

Commit all changes

```
git commit -a -m "commit message"
```

Commit individual changes

```
git commit -m "commit message" path/to/file.txt
```

