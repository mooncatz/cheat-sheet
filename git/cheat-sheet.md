# Git cheat sheet

## Edit a commit

Edit the latest commit

```bash
# Add new changes
git add edited.file

# Add these changes to the latest commit
git commit --amend -m "A new commit message that will replace the old commit message"
# or to keep the original commit message
git commit --amend --no-edit
```

## Project specific ssh keys

Clone using a specific ssh key

```bash
GIT_SSH_COMMAND="ssh -i ~/.ssh/id_rsa_example" git clone git@gitlab.com:workspace/repository.git
```

Set a default ssh key for the repository

```bash
git config core.sshCommand "ssh -i ~/.ssh/id_rsa_example"
```
