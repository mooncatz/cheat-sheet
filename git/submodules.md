# Git submodules

## Pull a submodule

```zsh
git submodule update

# git add and commit the changes
```


## Update a submodule

```zsh
git submodule update --remote

# git add and commit the changes
```

## Update a submodule to a specific commit

```zsh
cd <submodule>
git fetch
git checkout <commit>

cd ..
# git add and commit the changes
```
