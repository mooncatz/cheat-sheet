# Byobu command reference

## General

In `byobu`, the commands are the same as when using `screen` or `tmux`, except for what is listed below.

[GNU Screen command reference](https://gitlab.com/mooncatz/cheat-sheet/-/blob/master/screen/screen-command-reference.md)  
[Tmux command reference](https://gitlab.com/mooncatz/cheat-sheet/-/blob/master/tmux/tmux-command-reference.md)

## Basics

| command | description |
| --- | --- |
|`byobu`| start/reattach session |
|`byobu-select-backend`| select backend (screen opr tmux) |
|`byobu -S sessionname`| start new session named 'sessionname' |
|`byobu -ls`| list sessions |
|`byobu-enable`| start byobu at login (good for remote management if e.g. connection is lost) |
|`byobu-disable `| disable byobu from starting at login |
