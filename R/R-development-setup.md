# Setup R on MacOS

## 1. R version management

This section is not currently written.

Currently the best way to manage R version is to do it manually [https://docs.rstudio.com/resources/install-r/](https://docs.rstudio.com/resources/install-r/)

There are no "official" standalone tool for R version management.
A few open source projects exist. One very promising project is [renv-installer](https://github.com/jcrodriguez1989/renv-installer). The project is forked from pyenv and modified to work with R.

## 2. Install R

With homebrew:

```
brew install r
```

## 3. Install renv - project specific virtual environments

In short, renv for R is like venv for Python.

From within the R interpreter run:
```
install.packages("renv")
```

# Usage

## R

**Start R in your project:**

```
cd <project-directory>
R
```

**Install packages:**

```
install.packages("<package-name>")
```

**Remove packages:**

```
remove.packages("<package-name>")
```

## renv

Within a running R interpreter:

**Initialize a new environment:**

```
renv::init()
```

**Install packages:**

Install packages as you normally would.
This does not save the package version information (project state) to the lockfile! You must do that manually.

**Save project state (package version information) to the lockfile `renv.lock`:**

```
renv::snapshot()
```

**Restore the project state to that defined in `renv.lock`:**

This is useful if you install/update some package and it breaks your project.

```
renv::restore()
```

### renv note

When you run `renv::init()` for the first time you will be greted by the following one-time message which can be useful when you decide to uninstall R:

```
renv maintains a local cache of data on the filesystem, located at:

  - '~/Library/Application Support/renv'

This path can be customized: please see the documentation in `?renv::paths`.

renv will also write to files within the active project folder, including:

  - A folder 'renv' in the project directory, and
  - A lockfile called 'renv.lock' in the project directory.

In particular, projects using renv will normally use a private, per-project
R library, in which new packages will be installed. This project library is
isolated from other R libraries on your system.

In addition, renv will update files within your project directory, including:

  - .gitignore
  - .Rbuildignore
  - .Rprofile

Please read the introduction vignette with `vignette("renv")` for more information.
```
