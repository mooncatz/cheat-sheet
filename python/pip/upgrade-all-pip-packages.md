# Upgrade all pip packages

Upgrade all python packages in the envirinment with

```
pip list --outdated --format=freeze | grep -v '^\-e' | cut -d = -f 1 | xargs -n1 pip install -U
```

## Breakdown of the comman

Okay so what does this do? It is all very simple.


`pip list --outdated` outputs a list of packages that are outdated along with version numbers and some additional information that we are not interested in.

```
Package              Version    Latest       Type
-------------------- ---------- ------------ -----
bleach               3.1.0      3.1.5        wheel
cachetools           4.0.0      4.1.0        wheel
certifi              2019.11.28 2020.4.5.1   wheel
```

`pip list --outdated --format=freeze` formats the output to a requirements format. This is simply a list of "package==version"

```
bleach==3.1.0
cachetools==4.0.0
certifi==2019.11.28
```

`grep -v '^\-e'` takes the above output and selects only the stuff that do not beging with "-e". This is to skip editable package definitions.

`cut -d = -f 1` cuts the input at delimiter (the flag -d) "=" and select only the fields (the flag -f) at position 1.

```
bleach
cachetools
certifi
```

`xargs -n1 pip install -U` build and execute command lines from the input. The -n1 flag specifies to use one argument per command, so one package name per command. It executes

```
pip install -U bleach
pip install -U cachetools
pip install -U certifi
```
