# Pyenv - Python version management

[Pyenv](https://github.com/pyenv/pyenv) allows you to install multiple Python versions and run them side by side. You can choose:
1. which Python version to use globally, and
2. which Python version to use in your specific projects.

## Install pyenv

There are three major ways for installing pyenv: the [automatic installer](https://github.com/pyenv/pyenv#automatic-installer), basic [github checkout](https://github.com/pyenv/pyenv#basic-github-checkout), and the [homebrew installer](https://github.com/pyenv/pyenv#homebrew-in-macos)

### Ubuntu


```bash
# Install Python build dependencies (https://github.com/pyenv/pyenv/wiki#suggested-build-environment) in order to install a new python version. 
sudo apt install make build-essential libssl-dev zlib1g-dev libbz2-dev libreadline-dev libsqlite3-dev wget curl llvm libncursesw5-dev xz-utils tk-dev libxml2-dev libxmlsec1-dev libffi-dev liblzma-dev

# Use the automatic installer (https://github.com/pyenv/pyenv-installer) provided by pyenv

curl -L https://github.com/pyenv/pyenv-installer/raw/master/bin/pyenv-installer | bash

# Setup the shell variables

echo 'export PYENV_ROOT="$HOME/.pyenv"' >> ~/.profile
echo 'export PATH="$PYENV_ROOT/bin:$PATH"' >> ~/.profile
echo 'export PATH="$(pyenv root)/shims:$PATH"' >> ~/.profile
echo 'eval "$(pyenv init --path)"' >> ~/.profile
echo 'eval "$(pyenv init -)"' >> ~/.bashrc
source ~/.profile
source ~/.bashrc

# Finally, install the `python-is-python3`, package which just symlinks /usr/bin/python to python3.

sudo apt install -y python-is-python3
``` 

### MacOS

I will use Homebrew but the pyenv project's [pyenv-installer](https://github.com/pyenv/pyenv#the-automatic-installer)
and the [other installation options]([https://github.com/pyenv/pyenv#installation](https://github.com/pyenv/pyenv#installation)) are also very good installation options.

```bash
brew install pyenv

# Add `pyenv init` to your shell (see [pyenv installation](https://github.com/pyenv/pyenv#installation) for more details).

echo 'export PYENV_ROOT="$HOME/.pyenv"' >> ~/.zshrc
echo 'export PYENV_ROOT="$HOME/.pyenv"' >> ~/.zprofile
echo 'command -v pyenv >/dev/null || export PATH="$PYENV_ROOT/bin:$PATH"' >> ~/.zshrc
echo 'command -v pyenv >/dev/null || export PATH="$PYENV_ROOT/bin:$PATH"' >> ~/.zprofile
echo 'eval "$(pyenv init -)"' >> ~/.zshrc
echo 'eval "$(pyenv init -)"' >> ~/.zprofile

# Install [python build dependencies](https://github.com/pyenv/pyenv/wiki#suggested-build-environment) in order to install a new python version.

# Install Xcode Command Line Tools first if you have not done that. 

# Then install these
brew install openssl readline sqlite3 xz zlib
```

# Use pyenv

```bash
# List available python versions
pyenv install --list

# Install a Python vesrion
pyenv install 3.12.0

# List installed Python versions
pyenv versions

# Set the global Python version
pyenv global 3.12.0

# Set the local Python version
pyenv local 3.12.0
```