# Pipx

## Install pipx

It is good practice to not install anything extra to the system's python. Therfore, first install a new python version using pyenv, and then install pipx to the new python version.

### Ubuntu
```bash
# Using python 3.9.10 but you can choose another python version
pyenv install 3.9.10
pyenv global 3.9.10 
python -m pip install --user pipx
python -m pipx ensurepath  # adds to PATH
source ~/.profile
source ~/.bashrc
``` 

### MacOS

```bash
brew install pipx
pipx ensurepath
```

# Use pipx

```bash
pipx --help
```