# Pipenv - Python virtual environments

Pipenv combines pip and virtualenv into one tool.

Pipenv creates a separate virtual environment (by default in `~/.virtualenvs`) for each project directory in which you initialise pipenv. It also creates a `Pipfile` for defining project requirements, and a lockfile `Pipfile.lock` for specifying the exact versions for the package and sub-dependencies, such that the project is exactly reproducible.

## Recommendations

It is good practice not to install anything extra to the system's python. Therfore, first install a new python version using pyenv, then install pipx to the new python version.

* [Install pyenv](https://gitlab.com/mooncatz/cheat-sheet/-/blob/master/python/pyenv/setup-pyenv.md)
* [Install pipx](https://gitlab.com/mooncatz/cheat-sheet/-/blob/master/python/pipx/setup-pipx.md)


## Install pipenv

These instructions are OS independent.

```bash
pipx install pipenv
```

## Set up a python project

```bash
# Create a project
mkdir <project>
cd <project>

# Create a virtual environment for the project using a specific python version
pipenv --python 3.12.1

# Install python packages to the virtual environment
pipenv install numpy

# Activate the virtual environment
pipenv shell

# Deactivate the virtual environment
exit

# Remove the virtual environment
pipenv --rm

# Help
pipenv --help
```
