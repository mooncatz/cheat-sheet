# pipenv command reference

## Create a virtual environment using a specific python version

```bash
pipenv 3.12.0
pipenv --tree
pipenv --two
```

## Install packages

```bash
pipenv install tensorflow
pipenv install flask==0.12.1
```

## Run commands in the virtualenv
```bash
pipenv run ipython notebook
```
