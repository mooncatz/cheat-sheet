# React and Yarn commands cheat sheet
This cheat sheet assumes:  
  * you have used create-react-app.
  * you use react-gh-pages for the `deploy` and `predeploy` commands.

## Working directory
`cd` into the project directory (or your working directory) where your node/yarn packages are avalible.

### Avalible commands are listed in `package.json`
```
"start": "react-scripts start",
"build": "react-scripts build",
"test": "react-scripts test",
"eject": "react-scripts eject",
"predeploy": "yarn run build",
"deploy": "gh-pages -d build"
```

### Basic usage
Start development build
```
yarn start
```

Build production build (optimised for performance)
```
yarn build
```

Deploy a production build to github pages
```
yarn deploy
```
