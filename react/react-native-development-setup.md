# The complete React Native app development setup

These instructions try to give explanations to and simplify the setup of the development machine with the essential tools you need for React Native app development. Additionally I try to provide brief explanations to the steps i think the novel beginner might find poorly motivated or explained in the official documentation.

These instructions are based on the React Native [documentation](https://reactnative.dev/docs/environment-setup) as of May 2020.

# Table of contents
[[_TOC_]]

# 1 Setting up the development environment

I will show you how to install

* **Xcode** or **Android Studio**
* **Node.js**
* **Cocoapods** (required only for iOS app development)
* **React Native**

This will also be a great exercise to get comfortable working with command line tools, which opens up a whole new world of using your computer.

#### A note on Homebrew 🍺
I assume you use [**Homebrew**](https://brew.sh/). If you are not familiar with Homebrew it is, as described by themselves, "the missing package manager for macOS". It is a great tool for simplifying software management, and is used by many developers. All software packages you install with it are installed into the same single location, namely `/usr/local` (more specifically into `/usr/local/Cellar` and `/usr/local/Caskroom`). Homebrew itself is also elegantly installed in a single place `/usr/local/Homebrew`. Read more on the [faq](https://docs.brew.sh/FAQ#why-does-homebrew-prefer-i-install-to-usrlocal). Installing Homebrew is just a [one step installation process](https://brew.sh/). 

## 1.1 Install Xcode or Android Studio

### Xcode :apple:
You need [Xcode](https://developer.apple.com/xcode/) only if you want to compile and test apps for iPadOS or iOS.

### Android Studio 🤖
You need [Android Studio](https://developer.android.com/studio/) only if you want to compile and test apps for Android.

**On macOS:**  
Download the .dmg file and [install](https://developer.android.com/studio/install#mac) Android Studio.

**On Ubuntu:**  
Android studio is available as a snap package, or via the Software Center. Install it with 
```zsh
$ sudo snap install android-studio --classic
``` 
>(On Ubuntu) You can also download the .deb file from [developers.android.com](https://developer.android.com/studio/install#linux) but this I do not recommend this as it does not give you automatic updates. Also, installing the snap package makes it trivial to uninstall Android Studio if you decide to do so in the future.

## 1.2 Install Node :muscle:

[Node](https://nodejs.org/en/download/package-manager/) is the JavaScript runtime environment that executes the app. We will be programming in JavaScript and need Node for compiling the apps into iOS and/or Android apps.

**On macOS** the recommened way is to use [Homebrew](https://brew.sh): 
```zsh
$ brew install node
```

**On Ubuntu** you can install with 
```zsh
$ sudo apt install nodejs
``` 
or from the Software Center.

#### A note on npm and yarn :package:

Node comes with a CLI package manager called [npm](https://docs.npmjs.com/cli/npm). At the moment I prefer [yarn](https://yarnpkg.com/) (by facebook) because of its (to my understanding, still today) superior security, but this may change as npm was [recenty acquired by GitHub](https://github.blog/2020-04-15-npm-has-joined-github/) and I thus expect to see an improvment in npm.

## 1.3 Install Cocoapods :clipboard: (only if using Xcode)

The `react-native` package requires [CocoaPods](https://cocoapods.org/) for iOS app development as <a href="../resources/react-native-cocoapods.png" target="blank">seen when installing react-native</a>. For installation CocoaPods [recommend](https://guides.cocoapods.org/using/getting-started.html#installation) that you use the standard Ruby available on macOS unless you know what you're doing. We know what we are doing so we will use a Ruby version manager called [rbenv](https://github.com/rbenv/rbenv). 

[Chapter 2](#2-rbenv-optional) goes through the simple process of installing rbenv

Using the native Ruby is generally not recommended as it clutters up the system's Ruby with my own packages (gems), eventhough they would be `--user-install`s, and you can potentially break it. Instead I want install a separate ruby version that is isolated (all files confined in a single directory) and only used for development. This is what a Ruby version manager such as rbenv achieves.

Install CocoaPods gem according to the [instructions for installing gems](#install-gems) when using rbenv
```zsh
$ gem install cocoapods
```

## 1.4 Install React Native ⚛

**Note:** if you have an old version installed already:

>From the [docs](https://reactnative.dev/docs/environment-setup): React Native has a built-in command line interface. **Rather than install and manage a specific version of the CLI globally, we recommend you access the current version at runtime** using npx, which ships with Node.js. With npx react-native <command>, the current stable version of the CLI will be downloaded and executed at the time the command is run.  
This means you should not install react-native-cli globally, as is also stated in the docs: "If you previously installed a global react-native-cli package, please remove it as it may cause unexpected issues."

#### Using npx (npm)

Outside the project directory run
```zsh
$ npx react-native init MyApp
```

#### Using yarn

I have not found a good way to achieve the same directroy structure with yarn as with npx. This is because you with yarn first need to install `react-native` locally in your `projectDir` and then run `react-native init MyApp`, but that creates `MyApp` as a subfolder to `projectDir` (`projectDir/MyApp`). Therefore i advice you to use npx for now.

## That's it
You are now done. Happy coding.
![react native installation completed](https://gitlab.com/mooncatz/cheat-sheet/-/raw/master/resources/react-native-installation-complete.png "react native installation completed")

# 2 rbenv :gem: (optional)

The great convenience with rbenv is that each Ruby version is installed into its own directory under `~/.rbenv/versions`. This means that all ruby versions you install reside in the `~/.rbenv/versions` directory and **will not clutter** up your system with various installation files in multiple locations. This is exactly what we want.

>While following my guide please also read through the [rbenv installation instructions](https://github.com/rbenv/rbenv#installation) as they might change over time. These are the instructions as of May 2020.  

### 2.1 Install rbenv
Install with Homebrew:
```zsh
$ brew install rbenv
```
>Homebrew will also install the dependencies (packages that rbenv need) `autoconf`, `pkg-config`, `readline` and `ruby-build`. 

Then run `rbenv init` and follow the instructions, which are to add the following line to your `~/.bash_profile` (or to `~/.zprofile` if you use zsh).
```zsh
eval "$(rbenv init -)"
```
Details of what `rbenv init` does is in the [documentation](https://github.com/rbenv/rbenv#how-rbenv-hooks-into-your-shell). Basically it adds ~/.rbenv/shims to your $PATH, sources ~/.rbenv/completions/rbenv.bash (or .zsh) to enable autocompletion, rehashes shims (updates them), and enables commands like `rbenv shell`. You can always do all this manually by appropriately editing the ~/.bash_profile or ~/.zprofile yourself. Reaload your shell and download and run the rbenv-script to verify that the installation succeeded.
```zsh
$ source ~/.bash_profile
$ curl -fsSL https://github.com/rbenv/rbenv-installer/raw/master/bin/rbenv-doctor | bash
```
The output should be something like this 
```
Checking for `rbenv' in PATH: /usr/local/bin/rbenv
Checking for rbenv shims in PATH: OK
Checking `rbenv install' support: /usr/local/bin/rbenv-install (ruby-build 20200520)
Counting installed Ruby versions: none
  There aren't any Ruby versions installed under `/Users/otto/.rbenv/versions'.
  You can install Ruby versions like so: rbenv install 2.2.4
Checking RubyGems settings: OK
Auditing installed plugins: OK
```

### 2.2 Install a ruby version to use for development

List all avalible ruby versions
```zsh
$ rbenv install -l
```
Install a ruby version (currently latest stable release is 2.7.1). The optional `--verbose` flag simply makes rbenv output more status updates on what it is doing.
```zsh
$ rbenv install --verbose 2.7.1
```

### 2.3 Select what ruby version to use

Run the following to see which versions you have installed (the star `*` indicates what verison is selected)
```zsh
$ rbenv versions
```
Set a default (global) ruby version
```zsh
$ rbenv global 2.x.x.
```
**(optional)** Switch your shell session to use a installed ruby verison
```zsh
$ rbenv shell 2.x.x
```
**(optional)** Set a local (project directory) specific ruby version. This creates a `.ruby-version` file in that directory. This tells `rbenv` which ruby version to use when executed from within that directory.
```zsh
$ ruby local 2.x.x.
```

### 2.4 Install gems
Check the location where gems are being installed with `gem env`
```zsh
$ gem env home
```
Then, proceed to install gems as you normally would:
```zsh
$ gem install cocoapods
```

# 3 Removal instructions 🧨

No installation instructions are complete without instructions on how to uninstall the installed software packages.

### 3.1 Uninstall Xcode 
Uninstall like any other app from the app store. Move it to the trash.

### 3.2 Uninstall Android Studio
On Ubunut, `sudo snap remove android-studio` or remove it via the Software Center.

On macOS move it to the trash. To remove left overs take a look at [this](https://nektony.com/how-to/uninstall-android-studio-on-mac) and [stackoverflow](https://stackoverflow.com/questions/17625622/how-to-completely-uninstall-android-studio-on-mac).

### 3.3 Uninstall Node
On Ubuntu use `apt` or uninstall via the Software Center. See [apt manual](http://manpages.ubuntu.com/manpages/xenial/man8/apt.8.html) if you want to know more.

```zsh
$ sudo apt remove nodejs && sudo apt purge nodejs
```

On macOS `brew uninstall --force node` (see [this](https://docs.brew.sh/FAQ#how-do-i-uninstall-a-formula) and [this](https://docs.brew.sh/FAQ#how-do-i-uninstall-old-versions-of-a-formula)).

### 3.4 Uninstall CocoaPods

The following commands will completely uninstall CocoaPods:
```zsh
# remove all old versions of the gem
$ gem cleanup cocoapods

# choose which ones you want to remove
$ gem uninstall cocoapods
```

The same can be done also using version numbers:
```zsh
# remove all versions less than 2.7.1
$ gem uninstall cocoapods --version '<2.7.1'

# remove version 2.7.1 only
$ gem uninstall cocoapods --version 2.7.1
```

### 3.5 Uninstall rbenv

>These instructions are directly copied from the [rbenv documentation](https://github.com/rbenv/rbenv#uninstalling-ruby-versions).

**Uninstall Ruby versions:**

As time goes on, Ruby versions you install will accumulate in your `~/.rbenv/versions` directory.

To remove old Ruby versions, simply `rm -rf` the directory of the version you want to remove. You can find the directory of a particular Ruby version with the `rbenv prefix` command, e.g. `rbenv prefix 1.8.7-p357`.

The ruby-build plugin provides an `rbenv uninstall` command to automate the removal process.

**Uninstall rbenv:**

The simplicity of `rbenv` makes it easy to temporarily disable it, or uninstall from the system.

1. To **disable** rbenv managing your Ruby versions, simply remove the `rbenv init` line from your shell startup configuration (`~/.bash_profile` or `~/.zprofile`). This will remove rbenv shims directory from PATH, and future invocations like `ruby` will execute the system Ruby version, as before rbenv was installed.  
`rbenv` will still be accessible on the command line, but your Ruby apps won't be affected by version switching.

2. To **completely uninstall** rbenv:
Get the rbenv root path:
```zsh
$ rbenv root
/Users/username/.rbenv
```
perform step (1) and then remove its root directory (for me `/Users/username/.rbenv`). This will delete all Ruby versions that were installed under `<rbenv root>/versions/` directory:
```zsh
$ rm -rf <rbenv root>
```
As a final step perform the rbenv package removal. For instance, for Homebrew:
```zsh
$ brew uninstall rbenv
```

### 3.6 Remove React Native

React Native is something that only exist within your project. To remove a React Native project simply remove the project directory. There is no harm in keeping the react native project as the modules that make up rect native can only run when you execute them (e.g. using yarn) with node installed.
