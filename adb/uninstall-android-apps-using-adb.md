# Uninstall android apps using ADB

Download the [Android SDK Platoform-Tools](https://developer.android.com/studio/releases/platform-tools#downloads) from Google.

Then enable USB debugging on the Android device and connect it via USB to the computer.

```bash
cd Downloads/platform-tools

# Launch the adb shell
./adb shell
```

Inside the adb shell uninstall packages

```bash
# List all installed packages
pm list packages
# Uninstall a package
pm uninstall -k --user 0 <package name>
```

## Reinstall packages

The flag `--user 0` uninstalls the package for user 0. It is still available to other users. As such, packages can be reinstalled of user 0.

```bash
cmd package install-existing <package name>
```

# List of package names

## Samsung Bixby

```
com.samsung.android.bixby.wakeup
com.samsung.android.app.spage | Bixby homepage launcher
com.samsung.android.app.routines | Bixby Routines
com.samsung.android.bixby.service | Bixby features
com.samsung.android.visionintelligence | Bixby Vision
com.samsung.android.bixby.agent | Bixby Voice
com.samsung.android.bixby.agent.dummy | Bixby debug app
com.samsung.android.bixbyvision.framework | Bixby Vision
```

## ANT+ Service

The following apps are related to Bluetooth services and accessories like watches, fitness tracking, etc.

```
com.dsi.ant.sample.acquirechannels
com.dsi.ant.service.socket
com.dsi.ant.server
com.dsi.ant.plugins.antplus
```

## General System

```
com.android.egg | Android Easter Egg
com.sec.android.easyonehand | One hand mode
com.sec.android.widgetapp.samsungapps | Homescreen widget
com.sec.android.app.launcher | OneUI Launcher
com.samsung.android.mateagent | Galaxy Friends
com.sec.android.easyMover.Agent | Samsung Smart Switch
com.samsung.android.app.watchmanagerstub | Galaxy Watch
com.sec.android.daemonapp | Samsung Weather
com.samsung.android.app.social | What's New
com.samsung.ecomm.global | Samsung Shop
com.sec.android.app.voicenote | Voice Recorder
com.samsung.android.oneconnect | Smart Things
com.samsung.android.voc | Samsung Members
com.sec.android.app.popupcalculator | Samsung Calculator
com.sec.android.splitsound
com.google.android.feedback
com.google.android.googlequicksearchbox
com.google.android.onetimeinitializer
com.google.android.partnersetup
com.mobeam.barcodeService
com.android.providers.downloads.ui
com.android.providers.partnerbookmarks
com.android.sharedstoragebackup
com.android.vpndialogs
com.android.wallpaper.livepicker
com.android.wallpapercropper
com.samsung.android.app.dressroom | Samsung Wallpapers
com.samsung.android.scloud
com.samsung.android.sdk.handwriting
com.samsung.android.sdk.professionalaudio.utility.jammonitor
com.samsung.android.universalswitch
com.samsung.android.visioncloudagent
com.samsung.android.visionintelligence
com.samsung.android.widgetapp.yahooedge.finance
com.samsung.android.widgetapp.yahooedge.sport
com.samsung.app.highlightplayer
com.samsung.hiddennetworksetting
com.samsung.safetyinformation
com.samsung.storyservice
com.samsung.android.service.aircommand | Air command
com.samsung.android.app.aodservice | Always on Display
com.sec.android.app.dexonpc | Samsung Dex
com.samsung.android.ardrawing | AR Doodle
com.samsung.android.svoiceime
```

## Samsung Pay & Samsung Pass

```
com.samsung.android.samsungpassautofill | Samsung Auto fill
com.samsung.android.authfw | Samsung Authentication
com.samsung.android.samsungpass | Samsung Pass
com.samsung.android.spay | Samsung Pay
com.samsung.android.spayfw | Samsung Pay Framework
```

## Recreational Apps

```
com.google.ar.core | Google AR core for camera
flipboard.boxer.app | Flipboard app
com.samsung.android.wellbeing | Digital wellbeing
com.samsung.android.da.daagent | Dual Messenger
com.samsung.android.service.livedrawing | Live Message
```

## Samsung AR Emoji

```
com.samsung.android.aremoji | AR Emoji
com.sec.android.mimage.avatarstickers | Stickers for AR Emoji app
com.samsung.android.emojiupdater
```

## Samsung Sticker Center

```
com.samsung.android.app.camera.sticker.stamp.preload
com.samsung.android.stickercenter
com.samsung.android.stickerplugin
com.samsung.android.app.camera.sticker.facearframe.preload
com.samsung.android.app.camera.sticker.facearexpression.preload
com.samsung.android.app.camera.sticker.facear.preload
```

## Facebook

```
com.facebook.katana
com.facebook.system
com.facebook.appmanager
com.facebook.services
```

## Samsung Car Mode

```
com.samsung.android.drivelink.stub
```

## Printing Service Components

```
com.android.bips
com.google.android.printservice.recommendation
com.android.printspooler
```

## Samsung Email

```
com.samsung.android.email.provider
com.wsomacp
```

## Samsung Game Launcher & Settings

```
com.samsung.android.game.gamehome
com.enhance.gameservice
com.samsung.android.game.gametools
com.samsung.android.game.gos
com.samsung.android.gametuner.thin
```

## Samsung Browser

```
com.sec.android.app.sbrowser | Samsung Internet
```

## Samsung Gear VR

```
com.samsung.android.hmt.vrsvc
com.samsung.android.app.vrsetupwizardstub
com.samsung.android.hmt.vrshell
com.google.vr.vrcore
```

## Samsung Kids Mode

```
com.samsung.android.kidsinstaller
com.samsung.android.app.camera.sticker.facearavatar.preload | Camera stickers
com.sec.android.app.kidshome | Kids Home launcher
```

## Samsung LED Cover

```
com.samsung.android.app.ledbackcover
com.sec.android.cover.ledcover
```

## Edge Display

```
com.cnn.mobile.android.phone.edgepanel
com.samsung.android.service.peoplestripe | Edge panel plugin for contacts
com.samsung.android.app.sbrowseredge | Edge panel plugin for Samsung Internet
com.samsung.android.app.appsedge | App panel plugin for Edge display
```

## Samsung Dex

```
com.sec.android.desktopmode.uiservice
com.samsung.desktopsystemui
com.sec.android.app.desktoplauncher
```
