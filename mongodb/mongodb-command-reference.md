# Mongodb command reference

## Terminology

|mongo|relational|
|---|---|
| collection | table|

## Running mongodb
[running mongodb]([install and quickstart](https://docs.mongodb.com/manual/tutorial/install-mongodb-on-ubuntu/#install-mongodb-community-edition)

|Command|Description|
|---|---|
| `sudo systemstl start mongod` | start the mongod process|
| `sudo systemstl status mongod` | status of the mongod process |
| `sudo systemstl stop mongod` | stop the mongod process |
| `sudo systemstl restart mongod` | restart the mongod process |
| `mongo` | starts a mongo shell | 

