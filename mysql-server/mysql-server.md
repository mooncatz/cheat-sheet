[[_TOC_]]

# Setup MySQL server on Ubuntu or macOS

## Install mysql server
Install MySQL on macos:

```zsh
brew update
brew install mysql
```

Install MySQL on Ubuntu

```zsh
sudo apt update
sudo apt install mysql-server
```

## Start the MySQL server
```zsh
mysql.server start
```

## Secure the MySQL server instance
```zsh
mysql_secure_installation
```

You will be asked:  
* _Would you like to setup VALIDATE PASSWORD component?_
* _Please set the password for root here_
* _Remove anonymous users?_
* _Disallow root login remotely?_
* _Remove test database and access to it?_
* _Reload privilege tables now?_: **Yes**

## Login to MySQL server with the root user

Login to the MySQL server as root user:
```zsh
mysql -u root -p
```

## Create a database
```sql
CREATE DATABASE mydatabase;
```

## Create a non-root user
```sql
-- create user watcherdjango that can only connect via localhost
CREATE USER myuser@localhost IDENTIFIED BY `mysuperstrongpassword`;

-- verify that the user was created
SELECT user FROM mysql.user;
```

## Grant permissions
```sql
-- grant permissions on all tables (or specify specific tables) in mydatabase
GRANT SELECT, INSERT, UPDATE, DELETE, CREATE, DROP, REFERENCES, INDEX, ALTER ON mydatabase.* TO myuser@localhost;

-- verify priviliges
SHOW GRANTS FOR myuser@localhost;

-- if you later need you can revoke priviliges with syntax like this:
REVOKE SELECT ON mydatabase.* FROM myuser@localhost;
```
