[[_TOC_]]

# Install Tensorflow with CUDA and cuDNN on Ubuntu

> Updated in January 2020.

These are instructions for installing [the Nvidia software required by TensorFlow](https://www.tensorflow.org/install/gpu#software_requirements) to run on GPU. 

# Ubuntu 20 and Ubuntu 18

> These requirements are for TensorFlow 2.4.  
> For other TensorFlow versions, [check what CUDA and cuDNN versions you need](https://www.tensorflow.org/install/source#gpu)

As per the [software requirements for tensorflow 2.4](https://www.tensorflow.org/install/gpu#software_requirements) the following must be installed:
* Nvidia drivers 450.x or higher (required by CUDA 11)
* CUDA Toolkit 11.0 (required by Tensorflow 2.4)
* cuDNN 8.0.4


## Install Nvidia drivers

> **Note:** On Ubuntu 20, driver version 460 was needed for installing CUDA 11.0

View all devices which need drivers, and which packages (drivers) are available

```zsh
ubuntu-drivers devices
```

Install a driver. (You can also install it via the graphical tool Additional Drivers).

```zsh
sudo apt install --no-install-recommends nvidia-driver-460
```

After you reboot you can verify the install by listing gpu info with `nvidia-smi`.

## Install CUDA Toolkit 10.x or 11.x

These instructions are copied from the [CUDA download page](https://developer.nvidia.com/cuda-Toolkit-archive) **but:** 
* **Specify a cuda version** (since tensorflow 2.4 requires `cuda-11-0`) instead of using the default latest `cuda`.
* **Add `nvcc` to yor PATH.** (This has by some reason been left out from the download instructions but is [mentioned in the documentation](https://docs.nvidia.com/cuda/cuda-installation-guide-linux/index.html#environment-setup))

```bash
wget https://developer.download.nvidia.com/compute/cuda/repos/ubuntu2004/x86_64/cuda-ubuntu2004.pin
sudo mv cuda-ubuntu2004.pin /etc/apt/preferences.d/cuda-repository-pin-600
sudo apt-key adv --fetch-keys https://developer.download.nvidia.com/compute/cuda/repos/ubuntu2004/x86_64/7fa2af80.pub
sudo add-apt-repository "deb https://developer.download.nvidia.com/compute/cuda/repos/ubuntu2004/x86_64/ /"
```

Install `cuda-11-0`

```bash
sudo apt update
sudo apt install cuda-11-0
```

Add `nvcc` to your path

```bash
export PATH=/usr/local/cuda-11.0/bin${PATH:+:${PATH}}
```

Test that it works bu checking the version of the nvidia cuda compiler

```bash
nvcc -V
```

## Install cuDNN 

>**Note:** You can use `apt`, the package is called `libcudnn8`, but you will only get the latest version and we no not want the very latest version.

[Download](https://developer.nvidia.com/rdp/cudnn-archive) (requires login) the correct cuDNN verison for your setup.

Then follow the [installation instructions](https://docs.nvidia.com/deeplearning/cudnn/install-guide/index.html#installlinux-tar)

```bash
cd Downloads
# Extract the downloaded files
tar -xzvf cudnn-11.0-linux-x64-v8.0.4.30.tgz

# Copy the files
sudo cp cuda/include/cudnn*.h /usr/local/cuda/include
sudo cp cuda/lib64/libcudnn* /usr/local/cuda/lib64

# Set permissions
sudo chmod a+r /usr/local/cuda/include/cudnn*.h /usr/local/cuda/lib64/libcudnn*
```

Add to your LD_LIBRARY_PATH (needed by tensorflow)
```bash
export LD_LIBRARY_PATH="/usr/local/cuda/lib64:$LD_LIBRARY_PATH"
export LD_LIBRARY_PATH="/usr/local/cuda/include:$LD_LIBRARY_PATH"
```

## Install Tensorflow 2.4

```python
pip install tensorflow
```

## Test the installation

Test if tensorflow has proper acess to the gpu

```python
import tensorflow as tf
tf.config.list_physical_devices('GPU')
```

# Upgrade cuDNN

Multiple versions of cuDNN can coexist. See [upgrade instructions](https://docs.nvidia.com/deeplearning/cudnn/install-guide/index.html#upgrade).

