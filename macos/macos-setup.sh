#!/usr/bin/env zsh

# Homebrew
/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"

# VSCodium
brew install --cask vscodium

# Pyenv
brew update
brew install pyenv
echo 'export PYENV_ROOT="$HOME/.pyenv"' >> ~/.zshrc
echo 'export PYENV_ROOT="$HOME/.pyenv"' >> ~/.zprofile
echo 'command -v pyenv >/dev/null || export PATH="$PYENV_ROOT/bin:$PATH"' >> ~/.zshrc
echo 'command -v pyenv >/dev/null || export PATH="$PYENV_ROOT/bin:$PATH"' >> ~/.zprofile
echo 'eval "$(pyenv init -)"' >> ~/.zshrc
echo 'eval "$(pyenv init -)"' >> ~/.zprofile
brew install openssl readline sqlite3 xz zlib tcl-tk
exec "$SHELL"

# Pipx
brew install pipx
pipx ensurepath

# Pipenv
pipx install pipenv
echo 'export LANG="en_US.UTF-8"' >> ~/.zprofile

# nvm
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.1/install.sh | bash

# NodeJS
nvm install node
# Enables yarn, that comes bundled with node
corepack enable
