#!/usr/bin/env bash

cd ~

sudo apt update
sudo apt upgrade

sudo apt install -y gnome-tweaks byobu screen git synaptic curl make gcc build-essential

snap install codium --classic
snap install spotify

gsettings set org.gnome.shell.extensions.desktop-icons show-home false || echo "Failed to hide Home folder from desktop"
gsettings set org.gnome.shell.extensions.desktop-icons show-trash false || echo "Failed to hide Trash folder from desktop"
gsettings set org.gnome.nautilus.list-view use-tree-view true || echo "Failed to set tree view in nautilus"

# pyenv
sudo apt install -y make build-essential libssl-dev zlib1g-dev libbz2-dev libreadline-dev libsqlite3-dev wget curl llvm libncursesw5-dev xz-utils tk-dev libxml2-dev libxmlsec1-dev libffi-dev liblzma-dev
curl -L https://github.com/pyenv/pyenv-installer/raw/master/bin/pyenv-installer | bash
echo 'export PYENV_ROOT="$HOME/.pyenv"' >> ~/.profile
echo 'export PATH="$PYENV_ROOT/bin:$PATH"' >> ~/.profile
echo 'export PATH="$(pyenv root)/shims:$PATH"' >> ~/.profile
echo 'eval "$(pyenv init --path)"' >> ~/.profile
echo 'eval "$(pyenv init -)"' >> ~/.bashrc
sudo apt install -y python-is-python3
source ~/.profile
source ~/.bashrc

# pipx
pyenv install 3.9.10
pyenv global 3.9.10
python -m pip install --user pipx
python -m pipx ensurepath  # adds to PATH
source ~/.profile
source ~/.bashrc

# pipenv
pipx install pipenv

# Themes
cd ~/Downloads
wget https://github.com/vinceliuice/WhiteSur-gtk-theme/archive/refs/tags/2021-12-28.zip
unzip 2021-12-28.zip
~/Downloads/WhiteSur-gtk-theme-2021-12-28/install.sh --icon ubuntu --normal
# ~/Downloads/WhiteSur-gtk-theme-2021-12-28/install.sh --help

sudo apt install -y gnome-shell-extensions gnome-shell-extension-autohidetopbar
