# Reduce the size of gnome/gtk headerbar and titlebar in ubuntu 20

**Reduce the height of the headerbar of windows in Ubuntu 20.**

Result:

<img src="reduce-gnome-headerbar-titlebar-size-in-ubuntu-headerbars-2.png" alt="image of smaller headerbars" width="80%">

## How to

Create a file `~/.config/gtk-3.0/gtk.css` with the following content:

```css
/* Headerbar properties: for most windows */
headerbar {
  min-height: 0px;
  padding-top: 0px; /* allow elements to reach the top edge of the headerbar */
  padding-bottom: 0px; /* allow elements to reach the bottom edge of the headerbar */
  padding-right: 2px;
  padding-left: 2px;
}

/* Titlebar properties: for windows that do not use headerbar in the same way */
.titlebar {
  min-height: 0px;
  padding-top: 0px;
  padding-bottom: 0px;
  /*padding-left: 2px;  /* do not set these. Otherwise some apps will use both ".titlebar" and "headerbar" properties */
  /*padding-right: 2px;  /* do not set these. Otherwise some apps will use both ".titlebar" and "headerbar" properties */
}

/* The elements in the headerbar */
headerbar entry,
headerbar spinbutton,
headerbar button,
headerbar separator {
  margin-top: 2px; /* add some space above the elements in the headerbar */
  margin-bottom: 2px; /* add some space below the elements in the headerbar */
  margin-left: 0px; /* this is extra "space between" the elements in the headerbar */
  margin-right: 0px; /* this is extra "space between" the elements in the headerbar */
}
```

## Description

1. For the `headerbar` the top and bottom padding are set to zero. This allows the elements in the headerbar to reach all the way to the edge of the headerbar.
2. For the headerbar elements `entry`, `spinbutton`, `button` and `separator` a top and bottom margin are set to avoid the elements from appearing to reach all the way to the edge.

> This may seems like kind of a wierd way to apply paddings and margins in order to get a 2px space between the headerbar elements and the edge of the headerbar, and indeed, it is a bit wierd, but it is kind of a workaround to get it to work properly with all different apps. Not all GTK/Gnome apps use these css properties in the same way and therefore this workaround has to be done.

> Some windows seem to only have a headerbar, some only seem have a titlebar, and some seem have a headerbar inside a titlebar. Hence the incosistent way of changing the sizes of these.

## Caveats

1. Firefox: When using the **compact** layout density in Firefox there will be a small (4px) space at the top of the "tab selector". With "normal" and "touch" layout densities this will not happen.
2. Ubuntu software center: Does not seem to be affected by the changes.
