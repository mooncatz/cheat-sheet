# Chaining commands in bash

|operator|description|
|---|---|
|`;` | Chain commands together. Run the latter command no matter what happens in the first. Semicolon is the same as a new line.| 
|`&`| Run the second command immeadeately after the first command without waiting for the first command to finnish.| 
|`&&`| Run the second command iff the first command succeeds.|
|`||`| Run the second command iff the first command fails.| 

