# Table of contents
* [Install Homebrew](#install-homebrew)
* [Install gettext (and using it in virtualenv)](#install-gettext-and-using-it-in-virtualenv)
* [Compile languages (use gettext)](#compile-languages-use-gettext)

# Install gettext on macOS and use it with Django
These instructions where made on macOS Catalina using zsh but they should work in bash as well.

## Install Homebrew
Using Homebrew is by far the easiest way to cleanly install and cleanly uninstall gettext. Using Homebrew won't 
mess up or even touch macOS's default gettext packages as it installs everyting in an isolated folder.

Follow the instructions on https://brew.sh/  
```
/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
```

If you like to opt-out from Homebrew's analytics use  
```
brew analytics off
```

## Install gettext (and using it in virtualenv)
Install gettext with homebrew  
```
brew install gettext
```

After the installation Homebrew will give the following notice
>gettext is keg-only, which means it was not symlinked into /usr/local,
>because macOS provides the BSD gettext library & some software gets confused if both are in the library path.

In order to use gettext one needs to add that path variable that Homebrew is talking about. I will add it to my 
virtual environment so that it will be available from within my virtual environment.  

Add the following lines to `.virtualenvs/postactivate`

```
# This is for gettext to work (installed with Homebrew)
export TEMP_PATH=$PATH
export PATH=$PATH:/usr/local/Cellar/gettext/0.20.1/bin
```

Add the following lines to`.virtualenvs/postdeactivate`

```
# This is for gettext to work (installed with Homebrew)
export PATH=$TEMP_PATH
unset TEMP_PATH
```
 
## Compile languages (use gettext)
PyCharm will not (for some reason, at least for me) find gettext when you run `makemessages -l sv` from the `manage.py` task. Therefore 
you need to run the `manage.py` task yourself from within your virtualenvironment.  

Do the following. (I assume you have virtualenv and virtualenvwrapper installed. Although virtualenvwrapper is not 
necessary it is a nice set of shorthand commands.)

Activate your virtual environment:  
```
mooncatz@dyn-vpn ~ % workon your-virtual-environment
```

`manage.py` is located in the root of your project, so navigate there. For example `cd PycharmProjects/your-virtual-environment/`:  
```
(your-virtual-environment) mooncatz@dyn-vpn ~ % cd PycharmProjects/your-virtual-environment/
```  

You can double check that you are in the correct folder by listing the content of the folder with `ls`. 
My project folder contains:  
```
(your-virtual-environment) mooncatz@dyn-vpn your-virtual-environment % ls
README.md		backlog			requirements.txt
README_original.md	db.sqlite3		templates
__pycache__		locale			user
auction			manage.py		yaas
```

Now you can create the translations with `python manage.py makemessages -l sv`:
```
(your-virtual-environment) mooncatz@dyn-vpn your-virtual-environment % python manage.py makemessages -l sv  
Warning: Failed to set locale category LC_NUMERIC to en_FI.
Warning: Failed to set locale category LC_TIME to en_FI.
.
.
.
processing locale sv
.
.
.
```

And compile the translated messages with `python manage.py compilemessages`: 

```
(your-virtual-environment) mooncatz@dyn-vpn your-virtual-environment % python manage.py compilemessages
processing file django.po in /Users/mooncatz/PycharmProjects/your-virtual-environment/locale/sv/LC_MESSAGES
```
