# View the fonts used in a PDF document

Tested on MacOS Catalina. I suspect it will also work on Ubuntu.

## In terminal

Use `strings`

```
strings filename.pdf | grep FontName | cut -d '/' -f5
```

Extracts all strings from the PDF, selects only lines with FontName, shows the 5th field (`-f5`) from the ones separated by '/'. 
