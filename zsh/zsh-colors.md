# Give color and font style to the zsh shell

<img src="zsh-colors-random-colors.png" alt="randomly colored zsh" width="80%">

Change the following things in the `username@host Directory %` part of your prompt:

- foreground color (single or multiple)
- background color (single or multiple)
- text style (single or multiple)

To edit the colors edit the shell variable `PS1`, or one of the synonyms `PROMPT` or `prompt`, in `~/.zshrc` (create it if it does not already exist).  
A command reference for the zsh visual effects is found in the [zsh documentation](http://zsh.sourceforge.net/Doc/Release/Prompt-Expansion.html#Visual-effects). ASCI color codes are found [here](https://unix.stackexchange.com/questions/124407/what-color-codes-can-i-use-in-my-ps1-prompt/124409#124409)

### Generally

Customisation is done by wrapping the part of the the `$PROMPT` variable you want to customise with defferent [tags](http://zsh.sourceforge.net/Doc/Release/Prompt-Expansion.html#Visual-effects).

### Default prompt

The default zsh coloring is a good starting point.

```zsh
$ echo $PROMPT
%n@%m %1~ %#
```

The tag `%n` is the username, `@` is the @ character, `%m` is the hostname, `%~1` is to display the current directory ("path depth"=1), `%#` is to show a # for the root user instead of a % (as far as I have understood).

### Foreground color

Wrap the desired part with `%F{color-code}` and `%f`.

```zsh
export PROMPT="%F{41}%n@%m%f %1~ %#"
```

<img src="zsh-colors-foreground-color.png" alt="foreground color" width="80%">

### Background color

Similary wrap the desired part with `%K{color-code}` and `%k`.

```zsh
export PROMPT="%K{137}%n@%m%k %F{211}%1~%f %#"
```

<img src="zsh-colors-background-color.png" alt="background color" width="80%">

### Boldface

Wrap the desired part with `%B` and `%b`.

```zsh
export PROMPT="%F{171}%B%n@%m%b%f %F{184}%1~%f %"#
```

<img src="zsh-colors-boldface-color.png" alt="boldface color" width="80%">

### Random colors

Use `$RANDOM` to get a random positive 16 bit signed integer. Let's limit it to the range [28, 228] using `$((28 + $RANDOM % 200))`.

```zsh
export PROMPT="%F{$((28 + $RANDOM % 200))}%n@%m%f %F{$((28 + $RANDOM % 200))}%1~%f %# "
```

<img src="zsh-colors-random-colors.png" alt="practically colored zsh" width="80%">
