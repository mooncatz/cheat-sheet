# GNU Screen command reference

## Basics

| command | description |
| --- | --- |
|`screen`| start new session |
|`screen -ls`| list sessions |
|`crlt-a ?`| command reference |
|`ctrl-a c`| create new window |
|`crlt-a k`| kill window |
|`crlt-a d`| detach |
|`crlt-a -r`| reattach |
|`crlt-a -r XXXX`| reattach session with id XXXX|
|`crlt-a \`| quit and kill all windows |

## Navigation (basic)

| command | description |
| --- | --- |
|`ctrl-a n`| next window |
|`ctrl-a p`| previous window |
|`crtl-a crtl-a`| switch to most recent window |
|`crlt-a w`| list all open windows |
|`crlt-a 0`| go to window 0 (0 can be replaced with any number) |
|`crlt-a '`| go to window, a prompt is opened |
|`crlt-a "`| go to window, select from list |
|`crlt-a A`| Rename window title |

## Navigation (screen regions)

| command | description |
| --- | --- |
|`crlt-a S`| split on height |
|`crlt-a \|`| split on width |
|`crlt-a [tab]`| jump between regions |
|`crlt-a X`| destroy region (without killing the window) |
|`crlt-a Q`| destroy all regions except the current region (without killing windows) |

## Notifications & logging

| command | description |
| --- | --- |
|`crlt-a _`| Notify when there is no activity for 30 seconds |
|`crlt-a M`| Notify when there is activity |
|`crlt-a H`| create a logfile of all commands you run |



