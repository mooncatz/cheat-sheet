[[_TOC_]]

# Configure UFW firewall on Ubuntu server

>**Important**. In case you connect via ssh, add a rule to allow traffic on the port that you use for ssh before enabling the firewall.

By default ufw will deny all incoming traffic and allow all outgoing traffic.

## Allow traffic on a port

Allow traffic on port `1111`

```
sudo ufw allow 1111
```

## Allow ssh

Allow traffic on the ssh port listed in `/etc/services`, usually port `22` by default.

```
sudo ufw allow ssh
```

For added security you often do not use the default port. For example if your ssh server is listening on port `5555` you need to allow traffic on that port

```
sudo ufw allow 5555
```

## Enable UFW

Before enabling the firewall you can view the rules you have added with

```
sudo ufw show added
```

Enable the firewall with 

```
sudo ufw enable
```

When the firewall is active you can view its status and rules with

```
sudo ufw status
```

or 

```
sudo ufw status verbose
```
