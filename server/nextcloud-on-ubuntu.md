[[_TOC_]]

# Install and setup Nextcloud on Ubuntu 20

## 1. Install Nextcloud snap

```bash
sudo snap install nextcloud 
```
See [Change nextcloud data directory](#8-optional-change-nextcloud-data-directory-to-use-another-disk-partition) if you want to use a custom data directory. Optionally also see the section [Configure storage volumes using LVM](#7-optional-configure-storage-volumes-using-lvm).

## 2. Create admin user

```bash
sudo nextcloud.manual-install username password
```

## 3. Adjust domain names

Add the ip addres (or domain name if you have one) of the server to the trusted domain names

```bash
sudo nextcloud.occ config:system:set trusted_domains 1 --value=192.168.1.14
```

View all trusted domains

```bash
sudo nextcloud.occ config:system:get trusted_domains
```

## 4. Self-signed SSL certificate

Enable https by using a self-signed certificate. Web browsers will warn of a potential security risk since the ssl certificate is self-signed (not verified by a third party), but since it is your own server you can trust it.

```bash
sudo nextcloud.enable-https self-signed
```

## 5. Configure firewall

Now, that the nextcloud interface is encrypted (ssl) we can open up a port in the firewall. Nextcloud [listens to ports 80 an 443 ](https://github.com/nextcloud/nextcloud-snap#httphttps-port-configuration) by default. Let's open up the firewall for those ports. You can change also them if you like [https://github.com/nextcloud/nextcloud-snap#httphttps-port-configuration](https://github.com/nextcloud/nextcloud-snap#httphttps-port-configuration).

```bash
sudo ufw allow 80,443/tcp
```

## 6. Logging in

Now you can login to your Nextcloud server using your admin user via a web browser. for example, my nextcloud server is on my local network at [https://192.168.1.14](https://192.168.1.14)

## 7. (optional) Configure storage volumes using LVM

> In parts based on [RedHat's instructions](https://www.redhat.com/sysadmin/create-volume-group).

Two hard disk drives `/dev/sdb` and `/dev/sdc` will be used as storage in this example. They will be mirror images of each other in case one of them fails.

Some commands:
* `lsblk` - View the current storage configuration with 
* `sudo fdisk -l` - View the partition information
* `sudo pvs` - Display information about physical volumes
* `sudo vgs` - Display information about volume groups
* `sudo lvs` - Display information about logical volumes

### Create physical volumes

Initialize physical volumes for use with lvm

```bash
sudo pvcreate /dev/sdb
sudo pvcreate /dev/sdc
```

If you get an error like `Device /dev/sdb exluded by a filter` it is likely that there is an existing partition table on the disk. You can wipe the signatures with `sudo wipefs -a /dev/sdb` and then try `pvcreate /dev/sdb` again.

### Create volume group

Create a volume group named `storage-vg` that include both disks `/dev/sdb` and `/dev/sdc`

```bash
sudo vgcreate storage-vg /dev/sdb /dev/sdc
```

### Create logical volume

Create a logical volume `nextcloud-lv` of size `500 GB` with one mirror copy on each disk in the volume group `storage-vg`. Specifying `-m1` creates one mirror, which yields two copies of the file system. In case one of the disks break no data is lost.

```bash
sudo lvcreate -L 500G -m1 -n nextcloud-lv storage-vg
```

### Create a filesystem on the logical volume

Find what path you should use in the below command with `lsblk`.

```bash
sudo mkfs -t ext4 /dev/mapper/storage--vg-nextcloud-lv
```

The resulting configuration looks like this

```bash
sudo pvs
sudo vgs
sudo lvs
```

### Mount the logical volume automatically when booting

Use `sudo blkid` and note the UUID of the partition you want to automount. 

Edit `/etc/fstab` to tell the system to mount the logical volume at `/media/nextcloud` when booting.

```bash
UUID=the-uuid-of-the-logical-volume /media/nextcloud ext4 defaults 0 0
```

> View the documentation [https://help.ubuntu.com/community/Fstab](https://help.ubuntu.com/community/Fstab) for info on what the parameters mean.

Create mount point and mount all volumes sepcified in `/etc/fstab`

```bash
sudo mkdir /media/nextcloud
sudo mount -a
```

## 8. (optional) Change nextcloud data directory to use another disk partition

The instructions that follow are mostly copied from the [nextcloud wiki](https://github.com/nextcloud/nextcloud-snap/wiki/Change-data-directory-to-use-another-disk-partition).

### Connect the removable-media plug
The partition you want to use must be mounted somewhere in `/media/` or `/mnt/`. These are the only locations the snap can access under confinement with the `removable-media` plug. Connect the `removable-media` plug as mentioned in the [nextcloud snap readme](https://github.com/nextcloud/nextcloud-snap/blob/master/README.md) in order to grant the snap permission to access external drives.

```bash
sudo snap connect nextcloud:removable-media
```

### If you just installed the snap, and have not created an admin user yet

Then, create a data directory and change ownership and permissions of it

```bash
sudo mkdir -p /media/nextcloud/data
sudo chown -R root:root /media/nextcloud/data
sudo chmod 0770 /media/nextcloud/data
```

Update the Nextcloud config to use the new data directory by editing /var/snap/nextcloud/current/nextcloud/config/autoconfig.php and making sure the directory setting is pointing to the right place, e.g.

```php
...
'directory' => '/media/nextcloud/data',
...
```

Restart the php service

```bash
sudo snap restart nextcloud.php-fpm
```

### If you have already created an admin user

Stop the nextcloud snap for a moment

```bash
sudo snap stop nextcloud
```

Update the Nextcloud config to use the new data directory by editing `/var/snap/nextcloud/current/nextcloud/config/config.php`. Edit the `datadirectory` setting to point to the new data directory. In this example that will be `/media/nextcloud/data`.

```php
...
'directory' => '/media/nextcloud/data',
...
```

Move (or copy) the data directory

```bash
sudo mv /var/snap/nextcloud/common/nextcloud/data /media/nextcloud/
```

Restart the snap

```bash
sudo snap start nextcloud
```
