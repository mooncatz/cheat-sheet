# About

This is a mix of a cheat sheet and a guide for remote server management.

# Copy files over ssh

With ssh access you can use `scp` to copy files between the host and the client.

The general command is

```
scp <source> <destination>
```

To copy from the client to the remote host at `192.168.1.100` and port `1234` use

```
scp -P 1234 /path/to/source/on/client username@192.168.1.100:/path/to/destination/on/remote
```

As usual the paths can be either relative or absolute.
