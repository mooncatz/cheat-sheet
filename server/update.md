# Updating

## 1 Install new kernel

Upgrade to the latest stable kernel. Do not use `full-upgrade` since you want to keep the old kernel and its dependencies in case something goes wrong.
```
sudo apt upgrade linux-image-generic
```

## 2 Release upgrade

**Important:** Make sure there is enough space avalible on the disk partition the OS is installed on. If you use LVM read [lvm-collection.md](https://gitlab.com/mooncatz/cheat-sheet/blob/master/server/lvm-collection.md).  
Also, read the [Ubuntu server docs](https://ubuntu.com/server/docs/upgrade-introduction) for more information.

### 2.1 Generally

Upgrade to a stable Ubuntu release:
```
do-release-upgrade
```

Upgrade to a development release of Ubuntu:
```
do-release-upgrade -d
```

### 2.1 Upgrading an LTS release

On a LTS system `do-release-upgrade` will only upgrade to newer LTS releases (not to non-LTS releases). Furthermore, the upgrade will not be avalible (visible) until the first point release, i.e. not until the release of XX.04.1. This is in order to give time for final bugs to be fixed before the upgrade is made avalible for the LTS release.  

In order to upgrade an LTS system before the first point release you need to upgrade to a development release with `do-release-upgrade -d`.
