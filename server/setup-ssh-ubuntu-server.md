# Table of contents

  * [About](#about)
  * [1 Install Ubuntu Server](#1-install-ubuntu-server)
  * [2 Setup SSH](#2-setup-ssh)
    * [2.1 Generate ssh keys on the client that will be connecting to the host (the server).](#21-generate-ssh-keys-on-the-client-that-will-be-connecting-to-the-host-the-server)
    * [2.2 Transfer the keys to the host (the server)](#22-transfer-the-keys-to-the-host-the-server)
      * [2.2.1 On a trusted network](#221-on-a-trusted-network)
      * [2.2.2 On an untrused network](#222-on-an-untrused-network)
  * [3 Use SSH](#3-use-ssh)
  * [4 SSH security & deactivate password login](#4-ssh-security-deactivate-password-login)
    * [4.1 Edit ssh settings](#41-edit-ssh-settings)
      * [4.1.1 Add the following lines in the file](#411-add-the-following-lines-in-the-file)
      * [4.1.2 Description of what each setting does](#412-description-of-what-each-setting-does)
    * [4.2 Connect to a specific port](#42-connect-to-a-specific-port)
  * [5 Encrypted home folder](#5-encrypted-home-folder)

# About

This guide is written for Ubuntu and MacOS clients. The instructions covers some fundamental SSH operations so the instructions should still be valid for quite some time into the future. Tested with Ubuntu Server 18.04 LTS, and clients MacOS Catalina and Ubuntu Desktop 18.04 LTS.

**_Latest update 31 Jan 2020._**

## References

[https://help.ubuntu.com/community/SSH/OpenSSH/Keys](https://help.ubuntu.com/community/SSH/OpenSSH/Keys)  
[https://help.ubuntu.com/lts/serverguide/openssh-server.html](https://help.ubuntu.com/lts/serverguide/openssh-server.html)  
[https://wiki.centos.org/HowTos/Network/SecuringSSH](https://wiki.centos.org/HowTos/Network/SecuringSSH)
[http://manpages.ubuntu.com/manpages/xenial/man5/sshd_config.5.html](http://manpages.ubuntu.com/manpages/xenial/man5/sshd_config.5.html)


# 1 Install Ubuntu Server

Just follow the steps in the Ubuntu installer. Install OpenSSH if prompted, otherwise install it after the Ubuntu installation has completed.  

# 2 Setup SSH

It is important to harden the server's security settings. The first thing is to make SSH more secure.  

These instructions are valid for Ubuntu and MacOS clients.  

## 2.1 Generate ssh keys on the client that will be connecting to the host (the server).

The keys you will be generating will be stored in your home directory in `.ssh/id_rsa`.  

```
ssh-keygen -t rsa
```

By default 2048 bit keys are used (which are good). One can use 4096 bit keys with  

```
ssh-keygen -t rsa -b 4096
```

Protect the key with a password when prompted.

## 2.2 Transfer the keys to the host (the server)

There are a few ways one can transfere the ssh public keys (i.e. your credidentials) to the server so that you then can login to the server using the ssh keys and not username + password. I will show what I think are the two most straight forward ways. You will need the IP address of your server. On a local network you can for example login to your router and view it from the list of connected devices (or similar). There are also other straight forward methods for getting the IP address which you can easily find on the web.

### 2.2.1 On a trusted network
If you trust that the network you are on is secure, e.g. your local network, then this is the easiest method. The reason why you should trust the network is that you will use username + password login when transfeering the keys.  

On the client that will be connecting the the host run

```
ssh-copy-id <usernameOnTheServer>@<ip.address.to.host>
```

Follow the very simple instructions as prompted when prompted (login to server and transfer keys). 

### 2.2.2 On an untrused network

If you cannot trust the network, e.g. if you are connecting over the internet to your server without an encrypted connection, the use this method. This though requires physical access to the host. What you need to do is copy the **public key** file `id_rsa.pub` (of the client) to the server and concatenate it onto the `authorized_keys` file manually.

Copy the client's **public** ssh key `id_rsa.pub` to a memory stick (or other media) and mount it on the server (host). 

Backup the `authorized_keys` file if there are other keys you do not want to risk loosing if you screw up.

```
cp ~/.ssh/authorized_keys ~/.ssh/authorized_keys.backup
```

Then concatenate the ssh key on the memory stick to the `authorized_keys` file.

```
cat /path/to/memory/stick/id_rsa.pub >> ~/.ssh/authorized_keys
```

Inspect that the `authorized_keys` file is ok. Delete the backup file if you do not want to keep it.

# 3 Use SSH

Now that you have stored the public ssh keys of the client on the host (the server) you can securely connect to the host using your ssh credidentials instead of password. Connect using

```
ssh <usernameOnTheServer>@<ip.address.to.host>
```

It will ask for the password you protected your ssh key with.  

An example

```
ssh mooncatz@192.168.1.100
```

# 4 SSH security & deactivate password login

It is very important to disable the ability to remotely login to the server using the password login since the password you choose is not as secure as the 2048 bit (or 4096 bit) ssh key. We will also secure a few other things. 

**Before you proceed ensure you can login to the server using the ssh key.**

## 4.1 Edit ssh settings

Edit the sshd_config file that stores ssh settings with the nano editor

```
sudo nano /etc/ssh/sshd_config
```

### 4.1.1 Add the following lines in the file

```
PasswordAuthentication no 
PermitRootLogin no 	
Port XXXX
LoginGraceTime 1m
ClientAliveInterval 600
ClientAliveCountMax 0
Protocol 2
```

### 4.1.2 Description of what each setting does

`PasswordAuthentication no` disable password logins so that you are only able to remotely connect to the server using ssh keys.  

`PermitRootLogin no` disables the ability to login as root user via ssh (disables ssh root@<ip>). You should login as your normal user and use `sudo`. If you absolutely need to login as root user you should connect via ssh as your normal user and then swithc user.

`Port XXXX` only allow ssh connections on port XXXX (e.g. `Port 1330`) instead of the deafult `Port 20`. This protects agains automated malicious attempts that target default and commonly used ports.

`LoginGraceTime 1m` makes the server wait for credidentials for only 1 minute when someone attempts to login via ssh. After one minute the server closes the connection. You can set it to something shorter than 1 minute if you like.

`ClientAliveInterval 60` makes the server check every 60 seconds if the client is still connected. If the client is not connected the connection is closed by the server. This is good if you loose your connection or forget to logout.

`ClientAliveCountMax 0` makes the server accept 0 failures to connect to the client when checking if the client is alive/connected (the previous setting).

`Protocol 2` makes the server only use ssh protocol version 2. Avalible options are 1 and 2. Since version 1 is weaker only version 2 should be accepted.

## 4.2 Connect to a specific port

As the ssh port now is changed you need to specify it when connecting to the host

```
ssh <usernameOnTheServer>@<ip.address.to.host> -p XXXX
```

For example
```
ssh <usernameOnTheServer>@<ip.address.to.host> -p 1330
```

## 5 Encrypted home folder
