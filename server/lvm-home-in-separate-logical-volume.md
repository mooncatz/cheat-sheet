# LVM - home directory on separate logical volume

How to have your home directory on a separate logical volume on Ubuntu 18.04 LTS.

## Table of contents

[[_TOC_]]

## References and other guides

[https://help.ubuntu.com/community/Partitioning/Home/Moving](https://help.ubuntu.com/community/Partitioning/Home/Moving)  
[https://askubuntu.com/questions/923886/moving-home-with-lvm](https://askubuntu.com/questions/923886/moving-home-with-lvm)
[https://en.wikipedia.org/wiki/Logical_Volume_Manager_%28Linux%29](https://en.wikipedia.org/wiki/Logical_Volume_Manager_%28Linux%29)  
[https://wiki.ubuntu.com/Lvm](https://wiki.ubuntu.com/Lvm)  


# 1 Motivation

### Why use a logical volume and not a partition

Often you want to keep your home directory on a separate partition so that it is kept intact when upgrading the system or moving between distributions, for convenient encrypting, or for whatever reason you may have. Before LVM, having your home directory on a separate partition was the way to go, but this can be quite restrictive if you later decide to resize your partition. With LVM this is no more an issue. Logical volumes can be expanded and shrunk as you wish and they can span across multiple partitions and disks. So using a separate logical volume for your home directory instead of a separate (physical) partition makes storage management a dance on roses.

The concepts of LVM are:
* **Volume groups** - A collection Physical volumes
* **Physical volumes** - Storage devices and (physical) partitions. They provide the space to store logical volumes.
* **Logical volumes** - This is where your file system is. Logical volumes can span over multiple Physical volumes if you want to. (LVs are like virtual partitions)

# 2 Create a logical volume

### Preparation

Begin with viewing your current storage setup with these commands:
```
sudo lsblk
sudo vgdisplay
sudo pvdisplay
sudo lvdisplay
```

The `lsblk` shows your disk setup, `vgdisply` shows your volume group, `pvdisplay` shows your physical volumes, and `lvdisplay` shows your logical volumes.

You want to use `vgdisplay` and remember the name of the volume group (mine is ubuntu-vg).

```
mooncatz@erwintheserver:~$ sudo vgdisplay
  --- Volume group ---
  VG Name               ubuntu-vg
  ...
```

Below is my output of `lsblk`. I have one SSD on `sda`, and two HDDs on `sdb` and `sdc`.

```
mooncatz@erwintheserver:~$ lsblk
NAME                      MAJ:MIN RM   SIZE RO TYPE MOUNTPOINT
loop0                       7:0    0  89.1M  1 loop /snap/core/8268
loop1                       7:1    0  91.4M  1 loop /snap/core/8689
sda                         8:0    0 111.8G  0 disk
├─sda1                      8:1    0     1M  0 part
├─sda2                      8:2    0     1G  0 part /boot
└─sda3                      8:3    0 110.8G  0 part
  └─ubuntu--vg-ubuntu--lv 253:0    0     4G  0 lvm  /
sdb                         8:16   0   1.8T  0 disk
├─sdb1                      8:17   0     2G  0 part
└─sdb2                      8:18   0   1.8T  0 part
sdc                         8:32   0   1.8T  0 disk
├─sdc1                      8:33   0     2G  0 part
└─sdc2                      8:34   0   1.8T  0 part
```

### Create a logical volume

Create a new logical volume for the home directory. I create a 5GB logical volume `home-lv` to the my logical volume `ubuntu-vg`

```
sudo lvcreate -L 5G -n home-lv ubuntu-vg
```

### Format the logical volume

Format the logical volume. As my system's logical volume `ubuntu-lv` is formatted as ext4 i will use ext4 also for `home-lv`. Find what path you should use in the below command with `sudo lvdisplay`.

```
sudo mkfs -t ext4 /dev/mapper/ubuntu--vg-home--lv
```

# 3 Move the home directory to the logical volume

Now we will move the home directory to the new logical volume

### Mount the logical volume

Mount the logical volume to /mnt (for example). We mount it here to `/mnt` only temporarily as we copy the home directory to it. Later we will make it mount to `/home`, where it should be mounted.

```
sudo mount /dev/mapper/ubuntu--vg-home--lv /mnt
```

### Copy contents of /home to the logical volume

Copy everything from the home directory except .gvfs files to the logical volume

```
sudo rsync -aXS --progress --exclude='/*/.gvfs' /home/. /mnt/.
```

Validate that the copying worked

```
sudo diff -r /home /mnt -x ".gvfs/*"
```

### Backup your old home directory

Backup the old home directory by renaimg it. Then create a new home directory to which we mount the logical volume

```
cd /
sudo mv /home /old_home
sudo mkdir /home
```

# 4 Mount the logical volume to /home

Here we will make it so that the system automatically mounts the logical volume to `/home`

### Find the UUDI of the logical volume

First you need the UUID of the logical volume. You get it with `sudo blkid`

```
mooncatz@erwintheserver:~$ sudo blkid
/dev/mapper/ubuntu--vg-home--lv: UUID="ba0f71c7-6a2d-4443-b60d-b3e394a10aa5" TYPE="ext4"
```

### Tell system to mount the logical volume at /home

Edit /etc/fstab to tell the system to mount the logical volume at /home (instead of /mnt) when booting.

```
sudo nano /etc/fstab
```

Add the following line (replace with your own UUID) to the file and save it.

```
UUID=ba0f71c7-6a2d-4443-b60d-b3e394a10aa5 /home ext4 defaults 0 2
```

View the documentation [https://help.ubuntu.com/community/Fstab](https://help.ubuntu.com/community/Fstab) for info on what the parameters mean.

### Remount all volumes and/or partitions

First unmount the logical volume from `/mnt`

```
sudo umount /dev/mapper/ubuntu--vg-home--lv
```

Then remount all volumes and/or partitions

```
sudo mount -a
```

`cd /home` into the home directory to view that everything looks ok.

Inspect that the new volume is mounted at `/home` with `lsblk`

```
mooncatz@erwintheserver:~$ lsblk
NAME                      MAJ:MIN RM   SIZE RO TYPE MOUNTPOINT
loop0                       7:0    0  89.1M  1 loop /snap/core/8268
loop1                       7:1    0  91.4M  1 loop /snap/core/8689
sda                         8:0    0 111.8G  0 disk
├─sda1                      8:1    0     1M  0 part
├─sda2                      8:2    0     1G  0 part /boot
└─sda3                      8:3    0 110.8G  0 part
  ├─ubuntu--vg-ubuntu--lv 253:0    0     4G  0 lvm  /
  └─ubuntu--vg-home--lv   253:1    0     5G  0 lvm  /home
sdb                         8:16   0   1.8T  0 disk
├─sdb1                      8:17   0     2G  0 part
└─sdb2                      8:18   0   1.8T  0 part
sdc                         8:32   0   1.8T  0 disk
├─sdc1                      8:33   0     2G  0 part
└─sdc2                      8:34   0   1.8T  0 part
```

# 5 Reboot

If everything seems OK reebot the system to check that everything still works (e.g. ssh)

```
sudo reboot
```

# 6 Notes

Some notes

### You can use both a logical volume and a partition

You can of course still have a separate partition for your home directory. In that case you should create a Logical volume that uses the separate partition as a Physical device.

Another trick some people do is to partition their disk in, say, 10 (physical) partitions before setting up LVM (e.g. at the time of OS installation). This can make resizing the volume group (or rather the physical volumes) a bit easier. You can now let the Volume group use 5 of the partitions, leaving 5 partitions unused, not part of the volume group. If you later decide you need more storage you can just add additional partitions as phycial volumes to the volume group. This could also be convenient if you dual boot some non-LVM system, such as Windows.

### A note on resizing physical volumes

If you want to resize a Physical volume the `pvresize` command is your friend. Check out the [man pages](http://manpages.ubuntu.com/manpages/focal/man8/pvresize.8.html) and [this post](https://askubuntu.com/questions/252204/how-to-shrink-ubuntu-lvm-logical-and-physical-volumes) if you have trouble resizing. Having multiple (physical) partitions on the same disk can be convenient.
