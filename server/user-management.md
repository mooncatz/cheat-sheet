# User management on Ubunut

[Ubuntu docs](https://ubuntu.com/server/docs/security-users).

## Add a user

```
sudo adduser username
```

## Delete a user

```
sudo deluser username
```

Deleting an user account does not delete the user's home directory. If you later create a new user with the same UID/GID as the deleted user, the new user will have access to the deleted user's home directory. You may want to prevent this. You can do this by, for example, change the UID/GID values to e.g. the root account and also moving the directory.

```
sudo chown -R root:root /home/username/
sudo mkdir /home/archived_users/
sudo mv /home/username /home/archived_users/
```

## Lock and unlock a user's password

```
sudo passwd -l username
sudo passwd -u username
```

# Home directory permissions

[Ubuntu docs](https://ubuntu.com/server/docs/security-users).

By default home directories will have world-readable permissions, meaning that any user can read the contents. This is most often not desirable desirable.

Let's change the permissions of existing home direcotries, and then chane the default behaviour of `adduser` so that new user's home directories are not world-readable.

## Check current permissions

```
ls -ld /home/username
```

Permissions `drwxr-xr-x` mean that the directory is world-readable.

## Change permissions

You can remove the world readable permissions with

```
sudo chmod 0750 /home/username
```

## Change default permission of new users' home directory

Modify the `adduser` global default permissions when creating user home folders. Edit the `adduser` configuration file

```
sudo nano /etc/adduser.conf
```

Set

```
DIR_MODE=0750
```
