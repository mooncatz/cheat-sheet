[[_TOC_]]

# Install LaTeX

**Summary:** Shows how to install BasicTeX (only 110 MB), use the tex live package manager `tlmgr` to install packages as you need them, Use atom as the GUI editor with integrated pdf viewer.

The [recommended LaTeX distribution](https://www.latex-project.org/get/) for macos is [MacTeX](http://www.tug.org/mactex/). There are two versions Full MacTeX and BasicTeX (we will install BasicTeX). Key differneces:  
* [Full MacTeX](http://www.tug.org/mactex/mactex-download.html)
  * TeX Live full distribution with **over 4 GB** of material
  * GUI Applications: front ends, utilities, and a small amount of startup documentation
* [BasicTeX](http://www.tug.org/mactex/morepackages.html)
  * contains only the TeX Live piece of the full distribution, no GUI applications.
  * only **110 MB**

## Install BasicTeX
Install BasicTeX using [Homebrew](brew.sh). This makes it trivial to update it (or uninstall).
```
brew install --cask basictex
```

## How to install LaTeX packages as you need them
When you need a package for your document, say `siunitx`, simply install it via the terminal using the TeX Live package manager (comes with BasicTeX).
```
tlmgr install siunitx
```

## How to compile LaTeX document manually
You can always compile and build LaTeX documents into pdf manually if you like
```
cd /path/to/your/tex-document/directory
pdflatex mydocument.tex
```
