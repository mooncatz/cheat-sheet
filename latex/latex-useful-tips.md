# LaTeX tips

Center an **overly large** image (wider than available space) such that the amount it goes over the left margin is the same as the amount it goes over the right margin (think of zooming the image and keeping it centered).
```
\centerline{\includegraphics[width=2\linewidth]{image.png}}
```