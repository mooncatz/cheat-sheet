# Install the Java Development Kit (JDK)

[Difference between OpenJDK and Adoptium/AdoptOpenJDK](https://stackoverflow.com/questions/52431764/difference-between-openjdk-and-adoptium-adoptopenjdk)

1. Download Adoptium's distribution of the JDK from [https://adoptium.net/download/](https://adoptium.net/download/)
2. Extract and move to a suitable directory, e.g. `/opt`
    ```bash
    tar xzf <openjdk_binary>.tar.gz
    sudo mv <openjdk_binary>/jdk-<version_number> /opt
    ```
3. Add to PATH
    ```bash
    echo "export PATH=/opt/jdk-<version_number>/bin:$PATH" >> ~/.bashrc
    ```
4. Verify the installation
    ```bash
    soure ~/.bashrc
    java -version
    ```

> MacOS note: Adoptium OpenJDK can also be installed using homebrew. For the latest lts version `brew install --cask temurin`. See [https://adoptium.net/installation](https://adoptium.net/installation).

# Install Maven (optional)

1. [Download](https://maven.apache.org/download.cgi) Maven.
    ```bash
    # Download the latest release
    # At the moment of writing, Maven version 3.8.5 is the latest one 
    wget https://dlcdn.apache.org/maven/maven-3/3.8.5/binaries/apache-maven-3.8.5-bin.tar.gz
    ```
2. Verify the signature (optional).
    ```bash
    # Download the signature of the downloaded release
    wget https://downloads.apache.org/maven/maven-3/3.8.5/binaries/apache-maven-3.8.5-bin.tar.gz.asc
    # Download the public KEYS used by the Apache Maven developers
    wget https://downloads.apache.org/maven/KEYS
    # Import the downloaded KEYS
    gpg --import KEYS
    # Verify the signature
    gpg --verify apache-maven-3.8.5-bin.tar.gz.asc apache-maven-3.8.5-bin.tar.gz
    ```

    See [how to interpret the result](https://infra.apache.org/release-signing#valid-untrusted-vs-invalid-trusted). A valid signature should return a output like this
    
    ```bash
    gpg: Signature made la  5. maaliskuuta 2022 17.41.05 EET
    gpg:                using RSA key 1A2A1C94BDE89688
    gpg: Good signature from "Michael Osipov (Java developer) <1983-01-06@gmx.net>" [unknown]
    gpg:                 aka "Michael Osipov <michaelo@apache.org>" [unknown]
    gpg: WARNING: This key is not certified with a trusted signature!
    gpg:          There is no indication that the signature belongs to the owner.
    Primary key fingerprint: 6A81 4B1F 869C 2BBE AB7C  B727 1A2A 1C94 BDE8 9688
    ```
3. [Install](https://maven.apache.org/install.html) Maven.

    ```bash
    # Unpack the downloaded release
    tar xzvf apache-maven-3.8.5-bin.tar.gz
    # Move to a suitable install location
    sudo mv apache-maven-3.8.5 /opt/
    # Add to PATH
    echo "export PATH=/opt/apache-maven-3.8.5/bin:$PATH" >> ~/.bashrc
    ```
4. Verify the installation
    ```bash
    source ~/.bashrc
    mvn -v
    ```
