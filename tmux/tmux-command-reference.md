# Tmux command reference

## Basics

| command | description |
| --- | --- |
|`tmux`| start new session |
|`tmux list-sessions`| list sessions |
|`crlt-b ?`| command reference |
|`ctrl-b c`| create new window |
|`crlt-b &`| kill window |
|`crlt-b d`| detach |
|`tmux attach`| reattach session |
|`tmux attach -t sessionname`| reattach to session 'sessionname' (sessionname can also be the id)|
|`crlt-a \`| quit and kill all windows |

## Navigation (basic)

| command | description |
| --- | --- |
|`ctrl-b n`| next window |
|`ctrl-b p`| previous window |

## Navigation (screen regions)

| command | description |
| --- | --- |
|`crlt-b "`| split on height |
|`crlt-b %`| split on width |
|`crlt-b o`| jump between panes |
|`exit` | kill pane |

