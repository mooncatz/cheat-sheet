# Using Rosetta on MacOS

Run a command using a specific architecture

```bash
arch -arch [arch_name]
```

For more detail see

```bash
man arch
```

## VSCode

If developing in VSCode(ium), a convenient way of enabling rosetta in the terminal is by adding the following entries to the workspace's `settings.json`

```json
"terminal.integrated.profiles.osx": {
    "x86 zsh": {
        "path": "/usr/bin/arch",
        "args": ["-arch", "x86_64", "/bin/zsh"]
    }
},
"terminal.integrated.defaultProfile.osx": "x86 zsh"
```

Validate it by opening a new terminal and execute

```bash
arch
# returns i386 if rosetta is enabled
# returns arm64 if rosetta is disabled
```
